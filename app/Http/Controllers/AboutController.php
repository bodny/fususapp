<?php

namespace App\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;

class AboutController extends Controller
{
    /**
     * @return Renderable
     */
    public function __invoke()
    {
        return view('about');
    }
}

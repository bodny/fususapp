<?php

namespace App\Fuzzy\Memberships;

/**
 * Class LowOpinionMembershipFunction
 * @package App\Fuzzy\Memberships
 * @copyright (c) 2020
 * @author Tomas Bodnar <bodnarto@gmail.com>
 */
class LowOpinionMembershipFunction extends LMembershipFunction
{
    /**
     * LowOpinionMembershipFunction constructor.
     * @param float $min
     * @param float $max
     */
    public function __construct(float $min, float $max)
    {
        // maxValue = 2xbeta + alfa
        // alfa = 1/2 * beta
        $range = $max - $min;

        $beta = $range / 2.5;
        $alfa = $beta / 2;

        $a = $min + $beta;
        $a = $a - round(0.002*$max, 2);
        $b = $min + $beta + $alfa;
        $b = $b + round(0.002*$max, 2);

//        print_r('low opinion' . PHP_EOL);
//        print_r('min = ' . $min . PHP_EOL);
//        print_r('max = ' . $max . PHP_EOL);
//        print_r('alfa = ' . $alfa . PHP_EOL);
//        print_r('beta = ' . $beta . PHP_EOL);
//        print_r('a = min + beta = ' . ($min+$beta) . PHP_EOL);
//        print_r('b = min + beta + alfa = ' . ($min+$beta+$alfa) . PHP_EOL . PHP_EOL);
        print_r('a = ' . $a . PHP_EOL);
        print_r('b = ' . $b . PHP_EOL);
        print_r(PHP_EOL);
        print_r('----------------');
        print_r(PHP_EOL);


        parent::__construct($a, $b);
    }
}

#!/bin/bash

# start up project (app and db docker containers)
docker-compose --project-name=fususapp up --detach

# run migrations
docker exec -it fususapp_app bash -c "php artisan migrate"

# seed testing data
docker exec -it fususapp_app bash -c "php artisan db:seed --class=TestDataSeeder"
